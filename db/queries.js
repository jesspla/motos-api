const Pool = require('pg').Pool
const pool = new Pool({
  user: 'jessica-pla-7e3_motos',
  host: 'postgresql-jessica-pla-7e3.alwaysdata.net',
  database: 'jessica-pla-7e3_motos',
  password: 'motos1234',
  port: 5432,
})

const getMotos = (request, response) => {
    const marca = request.query.marca
    let selectQuery = 'SELECT * FROM motos'
    let params = []

    if(marca != undefined){
      params.push(marca)
      selectQuery += ' WHERE UPPER(marca) = UPPER($1)'
    }

    selectQuery += ' ORDER BY id ASC'

    pool.query(selectQuery, params, (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
}

const createMoto = (request, response) => {
  const { marca, modelo, year, precio } = request.body

  pool.query('INSERT INTO motos (marca, modelo, year, precio) VALUES ($1, $2, $3, $4)', [marca, modelo, year, precio], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`Moto con ID: ${results.insertId} añadida`)
  })
}

const updateMoto = (request, response) => {
  const id = parseInt(request.params.id)
  const { marca, modelo, year, precio } = request.body

  pool.query(
    'UPDATE motos SET marca = $1, modelo = $2, year = $3, precio = $4 WHERE id = $5',
    [marca, modelo, year, precio, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Moto con ID: ${id} modificada`)
    }
  )
}

const deleteMoto = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM motos WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`Moto con ID: ${id} eliminada`)
  })
}

module.exports = {
  getMotos,
  createMoto,
  updateMoto,
  deleteMoto,
}