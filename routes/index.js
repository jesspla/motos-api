var express = require('express');
var router = express.Router();
const db = require("../db/queries");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get("/motos" , db.getMotos)
router.post("/motos", db.createMoto)
router.put("/motos/:id", db.updateMoto)
router.delete("/motos/:id", db.deleteMoto)


module.exports = router;
